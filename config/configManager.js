//Process.env is a bit like a TestContext, in that is created for you. TEST_ENV is a user defined variable that will be set
//through command line by the build tool when tests are run.

//Here is full explanation on Process.env:
//The process.env global variable is injected by the Node at runtime for your application to use 
//and it represents the state of the system environment your application is in when it starts. 
//For example, if the system has a PATH variable set, this will be made accessible to you through process.env.PATH 
//which you can use to check where binaries are located and make external calls to them if required.

import {devConfig} from './devConfig';
import {sitConfig} from './sitConfig'; 


export function getConfig() 
{
    //Can't write process.env.Test_ENV.toLowerCase() since it is not defined. Hence have this constant.
    const test_env = process.env.Test_ENV || ""; 
    switch (test_env.toLowerCase()) {
        case 'dev':
            return devConfig;  
        case 'sit':
            return sitConfig; //This is the whole json object. To get the  baseUrl, you would use sitConfig.baseUrl (although that is all that is in the object jsut now)
        default:
            return sitConfig; //This is actually only one we have in reality. 
    }
}