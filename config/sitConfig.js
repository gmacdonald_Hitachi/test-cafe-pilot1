//This java script file contains a constant of type json object. 
//We could have called it var, but we don't want it changed so make it a const
//By using export, we are able to import it into any file in just the same way we would for a page object instance.
export const sitConfig = 
{
    baseUrl : 'https://www.staging.creditmaster3.co.uk/login'
};
