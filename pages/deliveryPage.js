import {Selector} from 'testcafe';
import BasePage from '../pages/basePage';

class DeliveryPage extends BasePage
{
    constructor()
    {
        super();
        //Element Locators
        this.continueButton = Selector("[data-test-id='compass-applicant-details-section-submit']");
    }

    //Methods
    async selectDeliveryType(t, appDetails)
    {
        var deliveryType = Selector("[type='button']").withExactText(appDetails.deliveryRequired);
        await t.click(deliveryType);
    }

    async clickContinueButton(t)
    {
        await t.click(this.continueButton);
    }

}

export const theDeliveryPage = new DeliveryPage();