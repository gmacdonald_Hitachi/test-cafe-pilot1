import {Selector} from 'testcafe';
import BasePage from '../pages/basePage';

class MessageToSalesPersonPage extends BasePage
{
    constructor() 
    {
        super()
        //Element locators
        this.esignAgreementButton = Selector("[type='button'][id='continue']");
    }

    //Methods
    async clickEsignAgreementButton(t)
    {
        await t.click(this.esignAgreementButton);
    }
}

export const theMessageToSalesPersonPage = new MessageToSalesPersonPage();