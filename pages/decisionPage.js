import {Selector } from 'testcafe';
import BasePage from '../pages/basePage';
class DecisonPage extends BasePage
{
    constructor()
    {
        super();
        //Element Locators
       this.proceedButton = Selector("[data-test-id='compass-application-decision-submit']").with({timeout: 30000});
       this.sendToCustomerButton = Selector("[data-test-id='compass-application-end-and-send']").with({timeout: 30000});
    }

    //Methods
    async checkDecisonAccepted(t)
    {
        const decisionAcceptedVisible = await Selector("[data-test-id='compass-application-decision-accepted']", { visibilityCheck: true })();
        return decisionAcceptedVisible;
    }

    async clickProceedButton(t)
    {
        await t.click(this.proceedButton);
    }

    async clickSendToCustomerButton(t)
    {
        await t.click(this.sendToCustomerButton);
    }
}

export const theDecisionPage = new DecisonPage();