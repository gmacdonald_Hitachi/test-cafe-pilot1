import {Selector} from 'testcafe';
import BasePage from '../pages/basePage';

class BankAccountPage extends BasePage
{
    constructor()
    {
        super();

        //Element Locators
        this.accountName = Selector("#compass_bankAccountDetails_name");
        this.sortCode = Selector("#compass_bankAccountDetails_sortCode");
        this.accountNumber = Selector("#compass_bankAccountDetails_accountNumber");
        this.continueButton = Selector("[data-test-id='compass-applicant-details-section-submit']");
        this.submitApplicatonButton = Selector("[data-test-id='compass-applicant-details-form-submit']");
    }

    //Methods
    async enterAccountDetails(t, appDetails)
    {
        await t
        .typeText(this.accountName, appDetails.accountName)
        .typeText(this.sortCode, appDetails.sortCode)
        .typeText(this.accountNumber, appDetails.accountNumber);
    }

    async clickContinueButton(t)
    {
        await t.click(this.continueButton);
    }

    async clickSubmitApplicationButton(t)
    {
        await t.click(this.submitApplicatonButton);
    }
}

export const theBankAccountPage = new BankAccountPage();