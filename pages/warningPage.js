import {Selector} from 'testcafe';
import basePage from '../pages/basePage';

class WarningPage extends basePage
{
    constructor()
    {
        super()
        
        //Element locators
        this.nextButton = Selector("[type='button'][id='continue']").with({timeout: 30000});
    }

    //Methods
    async clickNextButton(t)
    {
        await t.click(this.nextButton);
    }

}
export const theWarningPage = new WarningPage();
