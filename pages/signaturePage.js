import {Selector} from 'testcafe';
import BasePage from '../pages/basePage';

class signaturePage extends BasePage
{
    constructor()
    {
        super();
        
        //Element locators
        this.clickHereToSign = Selector(".to_click").with({timeout: 30000});
        this.confirmButton = Selector("[type='button'][id='continue']").with({timeout: 30000});
        this.modalConfirmButton = Selector("[id='popup-iaccept-continue'][type='button']").with({timeout: 30000});
    }

    //Methods
    async clickToSign(t)
    {
        console.log("in Clicktosign()");

        //Although there are 2 signatures, it looks like it reloads the page after you click on the first one
        //and there is only 1 signature available on the reload. So just select first available signature twice. 
        await t.click(this.clickHereToSign);
        await t.click(this.clickHereToSign);
    }

    async clickConfirmButton(t)
    {
        await t.click(this.confirmButton);
    }

    async clickModalConfirmButton(t)
    {
        await t
     //   .switchToIframe('#iframe') //Looks like it should be iframe, but it isn't
        .click(this.modalConfirmButton)
    //    .switchToMainWindow();
    }
    
}

export const theSignaturePage = new signaturePage();