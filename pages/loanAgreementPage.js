import {Selector} from 'testcafe';
import BasePage from '../pages/basePage';

class LoanAgreementPage extends BasePage
{
    constructor()
    {
        super();
        //Element locators
        this.clickHereToSign = Selector(".to_click").with({timeout: 45000});
        this.modalCompleteApplicationButton = Selector("[type='button'][id='popup-iaccept-complete']").with({timeout: 30000});
        this.confirmButton = Selector("[type='button'][id='continue']").with({timeout: 30000});

    }

    //Methods
    async clickToSign(t)
    {
        await t.click(this.clickHereToSign);
    }
    

    async clickConfirmButton(t)
    { 
        await t
        //.click(this.confirmButton)
        .click(this.modalCompleteApplicationButton);  
    }
}

export const theLoanAgreementPage = new LoanAgreementPage();