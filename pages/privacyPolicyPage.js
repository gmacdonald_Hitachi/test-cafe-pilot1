import {Selector} from 'testcafe';
import BasePage from './basePage'; //This uses a default export so don't need the curly backets

class PrivacyPolicyPage extends BasePage
{
    constructor()
    {
    //Calls BasePage construtor and so gives us access to the properties created in BasePage constructer (e.g. applicationDetails). 
    //If we did not have a constructor for this class, the BasePage one would be called inherently.
    //But, since we have effectivly overridden this default behaviour, we now need to explicity call the 
    //BasePage (superclass) constructor if we want it run.
    super();

    //Element locators
    this.ContineButton = Selector("[data-test-id='compass-privacypolicy-continue']");

    }
   
    //Methods
    async clickContinueButton(t)
    {
        await t.click(this.ContineButton);
    }

}

export const thePrivacyPolicyPage = new PrivacyPolicyPage();