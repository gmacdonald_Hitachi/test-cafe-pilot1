import {Selector  } from "testcafe";
import BasePage from "../pages/basePage";

class AddressDetailsPage extends BasePage
{
    constructor()
    {
        super();
        //Element Locators
        this.editButtons = Selector("[type='button']").withExactText("Edit");
        this.addressPostcode = Selector("[name='enteredPostcode']");
        this.timeAtAddressMonth = Selector("[name='fromMonth']");
        this.timeAdAddressYear = Selector("[name='fromYear']");
        this.findAddressButton = Selector("[data-test-id='compass_mainAddressDetails_mainAddress_entry0_submit']");
        this.selectAddressDropdown = Selector("[data-test-id='compass_mainAddressDetails_mainAddress_entry0_results']");
        this.selectAddressOptions = this.selectAddressDropdown.find("option");
        this.continueButton = Selector("[data-test-id='compass-applicant-details-section-submit']");
    }

    //Methods
    async clickPersonalDetailsEditButton(t, index)
    {
        await t.click(Selector(this.editButtons).nth(index));
    }

        async selectAddressStatus(t, appDetails)
    {
        var addressStatus = Selector ("[type='button']").withExactText(appDetails.addressStatus);
        await t.click(addressStatus);
    }

    async enterPostCode(t, appDetails)
    {
        await t.typeText(this.addressPostcode, appDetails.addressPostcode);   
    }

    async selectAddressFromResultsDropdown(t,appDetails)
    {
        await t
        .click(this.findAddressButton)
        .click(this.selectAddressDropdown)
        .click(this.selectAddressOptions.withText(appDetails.address));
    }
        
    async enterAddressFromDate(t, appDetails)
    {
        await t
        .typeText(this.timeAtAddressMonth, appDetails.atAddressFrom.month)
        .typeText(this.timeAdAddressYear, appDetails.atAddressFrom.year);
    }

    async clickContinueButton(t)
    {
        await t.click(this.continueButton);
    }
}

export const theAddressDetailsPage = new AddressDetailsPage();