import {Selector} from 'testcafe';
import {getConfig} from '../config/configManager'; //this is a function exported from configManager.js file

class BasePage{

    constructor()
    {
       //Always start the propery name with this. , otherwise it will not be defined properly

       //this.url = 'https://www.staging.creditmaster3.co.uk/login';

       //This will get the whole devConfig or sitConfig object. More useful when the object has many properties
       const CONFIG = getConfig(); 
       this.baseUrl = CONFIG.baseUrl; 

       //Could have said getConfig().baseURL, but having a CONFIG object is useful when the object has many properties
       //so you could access them more easily, e.g browser = CONFIG.browser 
 
       //Add the menu items here since there are available on all pages
       this.menuItemNewApplication = Selector("[data-test-id='compass-navbar-new-quote-action']");
        
    }

    //Methods    
    //Selects the nth element in array of elements found by given locator
    async selectNthElement(index, locator)
    {
        var element = Selector(locator).nth(index);
        return element;
    }
   

    CheckForMessageText(errorText, locator)
    {
        try
        {
            const errorMsg = Selector(locator)
            return (errorMsg.Text == element.innerText);
        }
        catch (err)
        {
            return false;
        }
    }

}

//BasePage needs to exported as the class (rather than an instance) because it is used to extend other classes.
export default BasePage;