import {Selector, t} from 'testcafe';
import BasePage from '../pages/basePage';
import XPathSelector  from '../utilities/xpath-selector';
//import xPathToCss from 'xpath-to-css';

class IncomeAndDependentsPage extends BasePage
{
    constructor()
    {
        super()

        //Element Locators
        this.totalAnnualIncome = Selector("#compass_financialDetails_incomeValue");
        this.continueButton = Selector("[data-test-id='compass-applicant-details-section-submit']");

    }
    //Methods
    async selectEmploymentStatus(t,appDetails)
    {
        var employmentStatus = Selector("[type='button']").withExactText(appDetails.employentStatus);
        await t.click(employmentStatus);
    }

    async enterTotalAnnualIncome(t, appDetails)
    {
        await t.typeText(this.totalAnnualIncome, appDetails.totalAnnualIncome);
    }

    async selectNumberOfDependent(t, appDetails)
    {
        var numOfDependents  = Selector("[type='button']").withExactText(appDetails.numberofDependents);
        await t.click(numOfDependents);
    }

    async selectSpouseEmploymentStatus(t, appDetails)
    {
        //Check if spouse employment status buttons are displayed and select value only if visible
       const spouseButtonsVisible = await Selector('#compass_financialDetails_spouseEmploymentStatus', { visibilityCheck: true })();

               
        if (spouseButtonsVisible)
        {   
            console.log("spouse buttons displayed");
               
             const spouseEmploymentStatusXpath = "//*[@id='compass_financialDetails_spouseEmploymentStatus']/button";
             const spouseEmploymentStatus = XPathSelector(spouseEmploymentStatusXpath).withExactText(appDetails.spouseEmploymentStatus);
   
             await t.click(spouseEmploymentStatus);
        }
       

    }
    async clickContinueButton(t)
    {
        await t.click(this.continueButton);
    }

}
export const theIncomeAndDependentsPage = new IncomeAndDependentsPage();