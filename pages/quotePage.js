import {Selector} from 'testcafe';
import BasePage, {basePage} from './basePage';

class QuotePage extends BasePage
{
    constructor()
    {
    //Calls BasePage construtor and so gives us access to the properties created in BasePage constructer (e.g. applicationDetails). 
    //If we did not have a constructor for this class, the BasePage one would be called inherently.
    //But, since we have effectivly overridden this default behaviour, we now need to explicity call the 
    //BasePage (superclass) constructor if we want it run.
    super();

        //Element locators
        this.goodsDescriptionField = Selector("[id='compass_goodsDescription']");
        this.totalCostField = Selector("[id='compass_loanParameters_goodsAmount']");
        this.depositField = Selector("[id='compass_loanParameters_deposit']");
        
        this.expectedMonthlyPaymentValue = Selector("[class='compass-cost-summary-amount']");
        this.continueButton = Selector("[data-test-id='compass-quote-form-submit']");
        //this.NumberOfPaymentsButton = Selector("[type='button']").withExactText(this.applicationDetails.numOfRepayments);
    }
    
    //Methods
    async enterAllQuoteDetails(t,appDetails)
    {
        this.enterGoodsDescription(t,appDetails);
        this.enterTotalCost(t,appDetails);
        this.enterDeposit(t,appDetails);
        this.clickNumberOfPaymentsButton(t,appDetails);
    }

    async enterGoodsDescription(t,appDetails)
    {
        await t.typeText(this.goodsDescriptionField, appDetails.goodsDescription);
    }

    async enterTotalCost(t,appDetails)
    {
        await t.typeText(this.totalCostField, appDetails.totalCost);
    }

    async enterDeposit(t,appDetails)
    {
        await t.typeText(this.depositField, appDetails.deposit);
    }

    //Element is found based on displayed text, which can change from one test to another as in based on input data.
    async clickNumberOfPaymentsButton(t,appDetails)
    {
        var numberOfPaymentsButton = Selector("[type='button']").withExactText(appDetails.numOfRepayments);
        await t.click(numberOfPaymentsButton);
    }

    async clickOnSubmitButton(t)
    {
        await t.click(this.continueButton);
    }


    //No awaits here, so don't need async (a bit suspicous of this though. Still need to wait for expectedMonthlyPaymentValue to 
    //be displayed to get its value. Maybe strictly speaking there should be an await here but get away without it since the element 
    //is already visible at this point since we have waited for several previous elemnts to be available)
    checkPaymentMatchesExected(t)
    {   
        var actualPayments = this.expectedMonthlyPaymentValue.innerText;
        var expectedPayments = applicationDetails.expectedMonthlyPayment;

        return  actualPayments == expectedPayments;
    }
}

//Instance of page object needs to be different from the classname. Easiest way is to make class upper case and instance lower case
export const theQuotePage = new QuotePage();
