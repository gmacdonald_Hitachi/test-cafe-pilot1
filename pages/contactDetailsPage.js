import {Selector} from 'testcafe';
import BasePage from './basePage';

class ContactDetailsPage extends BasePage
{
    constructor()
    {
        super();
        //Element Locators
        this.emailAddress = Selector("#compass_contactDetails_emailAddress");
        this.phoneNumber = Selector("#compass_contactDetails_phoneNumber");
        this.continueButton = Selector("[data-test-id='compass-applicant-details-section-submit']");

    }

    //Methods
    async enterContactDetails(t,appDetails)
    {
        await t
        .typeText(this.emailAddress, appDetails.emailAddress)
        .typeText(this.phoneNumber, appDetails.phoneNumber);
    }

    async clickContinueButton(t)
    {
        await t.click(this.continueButton);
    }
}

export const theContactDetailsPage = new ContactDetailsPage();