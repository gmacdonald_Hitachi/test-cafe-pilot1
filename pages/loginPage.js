import {Selector} from 'testcafe';
import BasePage from './basePage';

class LoginPage extends BasePage
{
    constructor(){
    
    //Calls BasePage construtor and so gives us access to the properites created in 
    //BasePage constructer (e.g. applicationDetails).If we did not have a constructor for this class, 
    //the BasePage one would be called inherently.But, since we have effectivly overridden this default 
    //behaviour, we now need to explicity call the BasePage (superclass) constructor if we want it run.
    super();

    //Element Locators
    this.retailerNameField = Selector ('#login-retailer');
    this.userNameField = Selector ('#login-username');
    this.passwordField = Selector('#login-password');
    this.signInButton = Selector('[data-test-id="compass-login-submit"]'); //No Id available here

    }

    //Methods   
    async loginAsRetailer(t, appDetails)
    {
        await t
        .typeText(this.retailerNameField, appDetails.retailerName, {paste: true})
        .typeText(this.userNameField, appDetails.userName, {paste: true})
        .typeText(this.passwordField, appDetails.password, {paste: true});
    } 
    
    async clickOnSignInButton(t)
    {

        await t.click(this.signInButton);
    }  

}

//Instance of page object needs to be different from the classname. 
//Easiest way is to make class upper case and instance lower case
export const theLoginPage =  new LoginPage();