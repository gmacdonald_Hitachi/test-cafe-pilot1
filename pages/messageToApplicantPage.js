import {Selector} from 'testcafe';
import BasePage from '../pages/basePage';

class MessageToApplicantPage extends BasePage
{
    constructor()
    {
        super();
        //Element locators
        this.ConfirmButton  = Selector("[type='button'][id='continue']").with({timeout: 30000});
    }

    //Methods
    async clickContinueButton(t)
    {
        await t.click(this.ConfirmButton);
    }
}

export const  theMessageToApplicantPage = new MessageToApplicantPage();