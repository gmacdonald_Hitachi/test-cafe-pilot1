import {Selector} from 'testcafe';
import BasePage from './basePage';

class PersonalDetailsPage extends BasePage
{
    constructor(){
        super();
        //Element locators
        this.firstName = Selector("#compass_personalDetails_firstName");
        this.middleName = Selector("#compass_personalDetails_middleName");
        this.lastName = Selector("#compass_personalDetails_lastName");
        this.formerName = Selector("#compass_personalDetails_formerName");
        this.dobDay = Selector("#compass_personalDetails_dateOfBirth_day");
        this.dobMonth = Selector("#compass_personalDetails_dateOfBirth_month");
        this.dobYear = Selector("#compass_personalDetails_dateOfBirth_year");
        this.continueButton = Selector("[data-test-id='compass-applicant-details-section-submit']");
    }


    //Methods    
    async enterMandatoryDetails(t,appDetails)
    {
        var titleButton = Selector("[type='button']").withExactText(appDetails.title);
        var maritalStatusButton = Selector("[type='button']").withExactText(appDetails.maritalStatus);

        await t
            .click(titleButton)
            .typeText(this.firstName, appDetails.firstName)
            .typeText(this.lastName, appDetails.lastName)
            .click(maritalStatusButton)
            .typeText(this.dobDay, appDetails.dateOfBirth.dobDay)
            .typeText(this.dobMonth, appDetails.dateOfBirth.dobMonth)
            .typeText(this.dobYear, appDetails.dateOfBirth.dobYear);      
    }

    async enterOptionalFields(t,appDetails)
    {

        if (appDetails.middleName !== null)
        {  
           await t.typeText(this.middleName, appDetails.middleName);
        }

        if (appDetails.formerName !== null)
        {  
           await t.typeText(this.formerName, appDetails.formerName);
        }

    }

    async clickContinueButton(t)
    {
        await t.click(this.continueButton);
    }
}

export const thePersonalDetailsPage = new PersonalDetailsPage();