import {Given, When, Then} from 'cucumber';
import {quotePage} from '../pages/quotePage'
import {privacyPolicyPage} from '../pages/privacyPolicyPage';


Then(/^I complete an application from the details in (.+?) test file$/, async t=>{
    await t.debug();

    //Enter Quote details
    quotePage.enterAll(t);  
    quotePage.clickNumberOfPaymentsButton(t);

    //Check Payment amount
    await t.expect(quotePage.checkPaymentMatchesExected).ok("Payment amount on Quote page does not match expected value");
        
    quotePage.clickOnSubmitButton(t);

    //Privacy Policy Page
    privacyPolicyPage.clickContinueButton(t);

    await t.debug();
});

