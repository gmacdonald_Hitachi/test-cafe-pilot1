import {Given, When, Then} from 'cucumber';
import {loginPage} from '../pages/loginPage';

Given(/^I have navigated to the logon page$/, async t =>{
        
  
    await t.navigateTo(loginPage.url);
    await t.maximizeWindow();


} );


When (/^I enter credentials from the (.+) test file$/, async t =>{
    await t.debug();
        
        loginPage.enterRetailerName(t);
        await t.maximizeWindow();
        loginPage.enterUserName(t);
        loginPage.enterPassword(t);
        
        loginPage.clickOnSignInButton(t);
        
} );
