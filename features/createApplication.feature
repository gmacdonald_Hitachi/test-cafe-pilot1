Feature: Create CM3 Application

   As a CM3 user
   I want to be able to create an application via CM3
   So that I can finance a purchase

Scenario Outline: Scenario Outline name: Happy path application creation
Given I have navigated to the logon page
When I enter credentials from the <fileName> test file
Then I complete an application from the details in <fileName> test file
Examples:
| fileName           |
| application.json   |