// This is in capitals in Freddie's example, why?
var CONFIG = 
{
    //As I understand it, process.env indicatest that what follows is a global variable set at runtime, probably by the CI tool
    //We are assumming that the tool sets the value of a variable called 'UI_URL'. I assume this name is user defined so need to agree
    //with whoever sets up the tool what the variable name will be.
    //  '|| 'localhost:5001' ' I assume means, if process.env.UI_URL not set, then use localhost:5001 as the base URL
    // I assuem this is for when the whole SUT is hosted locally (i.e. own laptop).

    baseUrl: process.env.UI_URL || 'localhost:5001'
}