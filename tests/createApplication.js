import {theLoginPage} from '../pages/loginPage';
import {theQuotePage} from '../pages/quotePage';
import {thePrivacyPolicyPage} from '../pages/privacyPolicyPage';
import {thePersonalDetailsPage} from '../pages/personalDetailsPage';
import {theContactDetailsPage} from '../pages/contactDetailsPage';
import {theAddressDetailsPage} from '../pages/addressDetailsPage';
import {theDeliveryPage} from '../pages/deliveryPage';
import {theIncomeAndDependentsPage} from '../pages/incomeAndDependentsPage';
import {theBankAccountPage} from '../pages/bankAccountPage';
import {theDecisionPage} from '../pages/decisionPage';
import {theWarningPage} from '../pages/warningPage';
import {theMessageToApplicantPage} from '../pages/messageToApplicantPage';
import {theMessageToSalesPersonPage} from '../pages/messageToSalesPersonPage';
import {theSignaturePage } from '../pages/signaturePage';
import {theLoanAgreementPage} from '../pages/loanAgreementPage';
    

fixture('Get a quote')
.page(theLoginPage.baseUrl)
.beforeEach
(async t => 
    {
    await t.setTestSpeed(0.9);
    }
);

//All input values come from the application.json files in the config folder
test('CreditMaster 3 Create Application test 1', async t => 
    {
        //Data input file.
        var applicationDetails = require('../appData/application.json');
       
        await t.maximizeWindow();

        await theLoginPage.loginAsRetailer(t, applicationDetails);
        await theLoginPage.clickOnSignInButton(t,applicationDetails);
                      
        await theQuotePage.enterAllQuoteDetails(t,applicationDetails);
        //Check Payment amount
        await t.expect(theQuotePage.checkPaymentMatchesExected).ok("Payment amount on Quote page does not match expected value");  
        await theQuotePage.clickOnSubmitButton(t);

        await thePrivacyPolicyPage.clickContinueButton(t);
     
        await thePersonalDetailsPage.enterMandatoryDetails(t,applicationDetails);
        await thePersonalDetailsPage.enterOptionalFields(t,applicationDetails);
        //await t.takeScreenshot();
        await thePersonalDetailsPage.clickContinueButton(t);

        await theContactDetailsPage.enterContactDetails(t,applicationDetails);
        await theContactDetailsPage.clickContinueButton(t,applicationDetails);

        await theAddressDetailsPage.selectAddressStatus(t,applicationDetails);
        await theAddressDetailsPage.enterPostCode(t,applicationDetails);
        await theAddressDetailsPage.selectAddressFromResultsDropdown(t,applicationDetails);
        await theAddressDetailsPage.enterAddressFromDate(t, applicationDetails);
        await theAddressDetailsPage.clickContinueButton(t);
        
        await theDeliveryPage.selectDeliveryType(t,applicationDetails);
        await theDeliveryPage.clickContinueButton(t);

        await theIncomeAndDependentsPage.selectEmploymentStatus(t, applicationDetails);
        await theIncomeAndDependentsPage.selectSpouseEmploymentStatus(t, applicationDetails)
        await theIncomeAndDependentsPage.enterTotalAnnualIncome(t, applicationDetails);
        await theIncomeAndDependentsPage.selectNumberOfDependent(t, applicationDetails);
        await theIncomeAndDependentsPage.clickContinueButton(t);

        await theBankAccountPage.enterAccountDetails(t, applicationDetails);
        await theBankAccountPage.clickContinueButton(t);
        await theBankAccountPage.clickSubmitApplicationButton(t);

        await t.expect(theDecisionPage.checkDecisonAccepted).ok("Decison page seems not to be displayed");
        console.log("decison page");
        await theDecisionPage.clickProceedButton(t);
       
        console.log("warning page");
        await theWarningPage.clickNextButton(t);

        console.log("message to applicant page");
        await theMessageToApplicantPage.clickContinueButton(t);

        //await t.debug();

        console.log("signature page");
        await theSignaturePage.clickToSign(t);
        await theSignaturePage.clickModalConfirmButton(t);

        console.log("message to salesperson page");
        await theMessageToSalesPersonPage.clickEsignAgreementButton(t);

        console.log("loan agreement page");
        await theLoanAgreementPage.clickToSign(t);
        await theLoanAgreementPage.clickConfirmButton(t);

        //await t.debug();
        
    }
);

//All input values come from the application.json file in the config folder
test('CreditMaster 3 Create Application test 2', async t => 
    {
        //Data input file.
        var applicationDetails = require('../appData/application2.json');
       
        await t.maximizeWindow();
        
        await theLoginPage.loginAsRetailer(t, applicationDetails);
        await theLoginPage.clickOnSignInButton(t,applicationDetails);
                      
        await theQuotePage.enterAllQuoteDetails(t,applicationDetails);
        //Check Payment amount
        await t.expect(theQuotePage.checkPaymentMatchesExected).ok("Payment amount on Quote page does not match expected value");  
        await theQuotePage.clickOnSubmitButton(t);

        await thePrivacyPolicyPage.clickContinueButton(t);
     
        await thePersonalDetailsPage.enterMandatoryDetails(t,applicationDetails);
        await thePersonalDetailsPage.enterOptionalFields(t,applicationDetails);
        //await t.takeScreenshot();
        await thePersonalDetailsPage.clickContinueButton(t);

        await theContactDetailsPage.enterContactDetails(t,applicationDetails);
        await theContactDetailsPage.clickContinueButton(t,applicationDetails);

        await theAddressDetailsPage.selectAddressStatus(t,applicationDetails);
        await theAddressDetailsPage.enterPostCode(t,applicationDetails);
        await theAddressDetailsPage.selectAddressFromResultsDropdown(t,applicationDetails);
        await theAddressDetailsPage.enterAddressFromDate(t, applicationDetails);
        await theAddressDetailsPage.clickContinueButton(t);
        
        await theDeliveryPage.selectDeliveryType(t,applicationDetails);
        await theDeliveryPage.clickContinueButton(t);

        await theIncomeAndDependentsPage.selectEmploymentStatus(t, applicationDetails);
        await theIncomeAndDependentsPage.selectSpouseEmploymentStatus(t, applicationDetails)
        await theIncomeAndDependentsPage.enterTotalAnnualIncome(t, applicationDetails);
        await theIncomeAndDependentsPage.selectNumberOfDependent(t, applicationDetails);
        await theIncomeAndDependentsPage.clickContinueButton(t);

        await theBankAccountPage.enterAccountDetails(t, applicationDetails);
        await theBankAccountPage.clickContinueButton(t);
        await theBankAccountPage.clickSubmitApplicationButton(t);

        await t.expect(theDecisionPage.checkDecisonAccepted).ok("Decison page seems not to be displayed");
        console.log("decison page");
        await theDecisionPage.clickProceedButton(t);
       
        console.log("warning page");
        await theWarningPage.clickNextButton(t);

        console.log("message to applicant page");
        await theMessageToApplicantPage.clickContinueButton(t);

        //await t.debug();

        console.log("signature page");
        await theSignaturePage.clickToSign(t);
        await theSignaturePage.clickModalConfirmButton(t);

        console.log("message to salesperson page");
        await theMessageToSalesPersonPage.clickEsignAgreementButton(t);

        console.log("loan agreement page");
        await theLoanAgreementPage.clickToSign(t);
        await theLoanAgreementPage.clickConfirmButton(t);

        //await t.debug();
        
    }
);

